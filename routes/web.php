<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Homecontroller@index');

Route::get('/page/{id}', 'Homecontroller@page');

Route::get('contact-us', 'ContactUSController@index');

Route::post('contact-us/sendmessage', 'ContactUSController@sendMessage');

Route::post('contact-us/sendmessage/ajax', 'ContactusController@sendMessageAjax');

Route::get('special/{id}', 'HomeController@specialEntry');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group([
    'middleware' => 'auth'
], function(){
    Route::resource('admin/specials', 'SpecialsController');
});
