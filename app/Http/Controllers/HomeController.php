<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Special;

class HomeController extends Controller
{
    public function index(){

        $pages = Page::all();

        $pageDetail = Page::where('id',1)->first();

        $specials = Special::all();

        return view('website.home', ['pages' => $pages, 'pageDetail' => $pageDetail, 'specials' => $specials]);
    }

    public function page($pageId){

        $pages = Page::all();

        $pageDetail = Page::where('id',$pageId)->first();

        return view('website.home', ['pages' => $pages, 'pageDetail' => $pageDetail]);
    }

    public function specialEntry($id){

        $special = Special::where('id', $id)->first();

        return view('website.special-entry', ['special' => $special]);
    }
}
