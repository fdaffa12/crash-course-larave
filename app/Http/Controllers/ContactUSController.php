<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use Mail;
use Validator;
class ContactUSController extends Controller
{
    public function index(){

        $pages = Page::all();
        
        return view('website.contact', ['pages' => $pages]);
    }

    public function sendMessage(Request $request){

        $input = $request->all();

        $validator = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            
        ]);

        Mail::send('mails.contactus', ['nameInput' => $input['name'], 'messageInput' => $input['message']], function($m){
            $m->from('postmaster@sandboxf104ed9ce0fe41878f4b95c5d0460238.mailgun.org', 'Hallo Ada Pesan');
            $m->to('faizdaffa17@gmail.com');
        });
    }

    public function sendMessageAjax(Request $request){
        $input = $request->all();

        $validator = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            
        ]);

        Mail::send('mails.contactus', ['nameInput' => $input['name'], 'messageInput' => $input['message']], function($m){
            $m->from('postmaster@sandboxf104ed9ce0fe41878f4b95c5d0460238.mailgun.org', 'Hallo Ada Pesan');
            $m->to('faizdaffa17@gmail.com');
        });

        return [
            'success' => true,
            'message' => 'Thank you, we will get back you as soon as possible'
        ];
    }
}
