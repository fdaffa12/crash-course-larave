<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>Document</title>
</head>
<body>
    <div>
        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRC1nKfkC7nX7r_uZD1Jzx0_ks6hd0ShK5ZFw&usqp=CAU" alt="" height="40">
    </div>
    <!-- A grey horizontal navbar that becomes vertical on small screens -->
    <nav class="navbar navbar-expand-sm bg-light">

        <!-- Links -->
        <ul class="navbar-nav">
        @foreach ($pages as $page)
        <li class="nav-item">
            <a class="nav-link" href="/page/{{$page->id}}">{{$page->name}}</a>
        </li>
        @endforeach
        <li class="nav-item">
            <a class="nav-link" href="/contact-us">Contact Us</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/admin/specials">Add/Edit/Remove Special</a>
        </li>
        </ul>
    
    </nav>

    <div class="row">
        @foreach ($specials as $special)
        <div class="col-md-3">
            <div style="text-align: center">
                <h4>{{$special->name}}</h4>
                <p>{{$special->was_price}}</p>
                <p>{{$special->current_price}}</p>
                <a href="/special/{{$special->id}}">Click here to view special</a></div>
            </div>
        </div>
        @endforeach
    <div>
        {{$pageDetail->description}}
    </div>
</body>
</html>