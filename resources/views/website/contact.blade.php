<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" href="/css/website.css">

    
    <title>Document</title>
</head>
<body>
    <div id="app">
        <div>
            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRC1nKfkC7nX7r_uZD1Jzx0_ks6hd0ShK5ZFw&usqp=CAU" alt="" height="40">
        </div>
        <!-- A grey horizontal navbar that becomes vertical on small screens -->
        <nav class="navbar navbar-expand-sm bg-light">
    
            <!-- Links -->
            <ul class="navbar-nav">
            @foreach ($pages as $page)
            <li class="nav-item">
                <a class="nav-link" href="/page/{{$page->id}}">{{$page->name}}</a>
            </li>
            @endforeach
            <li class="nav-item">
                <a class="nav-link" href="/contact-us">Contact Us</a>
            </li>
            </ul>
        
        </nav>

        <contact-us-form></contact-us-form>

{{--         
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form action="/contact-us/sendmessage" class="was-validated" method="POST">
    
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
    
                        @csrf
                        
                        <div class="form-group">
                          <label for="uname">Name:</label>
                          <input type="text" class="form-control" id="uname" placeholder="Enter your name" name="name">
                        </div>
                        <div class="form-group">
                          <label for="uname">Email:</label>
                          <input type="text" class="form-control" id="uname" placeholder="Enter your email" name="email">
                        </div>
                        <div class="form-group">
                          <label for="uname">Message:</label>
                          <textarea class="form-control" name="message" placeholder="Please enter your message here" id="" cols="30" rows="10"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div> --}}


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="/js/website.js"></script>
</body>
</html>
