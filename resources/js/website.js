// const { Value } = require('sass');

require('./bootstrap');

window.Vue = require('vue');

Vue.component('contact-us-form', require('./components/ContactForm.vue').default);

const app = new Vue({
    el: '#app'
});